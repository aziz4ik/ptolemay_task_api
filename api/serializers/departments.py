from rest_framework import serializers

from api.models import Department
from django.db import models

class DepartmentSerializer(serializers.ModelSerializer):
    employees_count = serializers.SerializerMethodField()
    total_salary = serializers.SerializerMethodField()


    class Meta:
        model = Department
        fields = (
            'id',
            'name',
            'employees_count',
            'total_salary'
        )

    def get_employees_count(self, obj):
        return obj.employees.count()

    def get_total_salary(self, obj):
        return obj.employees.aggregate(total_salary=models.Sum('salary'))['total_salary'] or 0