from django.contrib.auth.models import User
from django.db import models
from django.db.models import CASCADE, PROTECT


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(
                                   to=User,
                                   on_delete=PROTECT,
                                   related_name='%(class)s_createdby',
                                   null=True,
                                   blank=True)
    modified_by = models.ForeignKey(
                                    to=User,
                                    on_delete=PROTECT,
                                    related_name='%(class)s_modifiedby',
                                    null=True,
                                    blank=True)

    class Meta:
        abstract = True


class Department(BaseModel):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Employee(BaseModel):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    paternal_name = models.CharField(max_length=255)
    position = models.CharField(max_length=255)
    salary = models.DecimalField(max_digits=10, decimal_places=2)
    age = models.PositiveIntegerField()
    department = models.ForeignKey(Department, CASCADE, related_name='employees')

    def __str__(self):
        return f'{self.last_name} {self.first_name} {self.paternal_name}'
