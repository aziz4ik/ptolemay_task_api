from django.urls import path, include
from rest_framework import routers

from api.swagger import schema_view
from api.views import EmployeeViewSet, DepartmentViewSet

router = routers.DefaultRouter()
router.register('employees', EmployeeViewSet, 'employees')
router.register('departments', DepartmentViewSet, 'departments')


urlpatterns = [
    path('', include(router.urls)),
]
