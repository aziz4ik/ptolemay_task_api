from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated

from api.models import Employee, Department
from api.serializers.departments import DepartmentSerializer
from api.serializers.employees import EmployeeSerializer
from django_filters.rest_framework import DjangoFilterBackend


class EmployeeViewSet(viewsets.ModelViewSet):
    """
    Depending on HTTP method gives suitable action
    *GET* - returns list of employees
    *POST* - creates a new employee
    *PUT {id}* - updates employee when id of employee is sent
    *DELETE {id}* - deletes employee when id is sent

    """
    model = Employee
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['last_name', 'department']
    pagination_class = PageNumberPagination


class DepartmentViewSet(viewsets.ModelViewSet):
    """
    Depending on HTTP method gives suitable action
    *GET* - returns list of employees
    *POST* - creates a new employee
    *PUT {id}* - updates employee when id of employee is sent
    *DELETE {id}* - deletes employee when id is sent

    """
    model = Department
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer
    pagination_class = PageNumberPagination
